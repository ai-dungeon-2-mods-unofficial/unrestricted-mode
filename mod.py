from modloader import BaseMod
from story.utils import console_print

class UnrestrictedMode(BaseMod):
    def try_handle_command(self, command, args, generator, story_manager):
        if command == '!':
            text = ' '.join(args).replace('\\n','\n')
            result = story_manager.act(text)
            console_print(result)
            return True
        return False
    
    def instructions(self):
        return ['\n  "/! <text>" Append <text> to the story, without any processing.']
